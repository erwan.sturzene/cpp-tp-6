//
// Created by beatw on 3/3/2022.
//


#include "../includes/Catch2/catch.hpp"
#include "../src/Point.h"

TEST_CASE("Point: Construction") {
    Point p(10, 12);

    REQUIRE(10 == Approx(p.getX()));
    REQUIRE(12 == Approx(p.getY()));
    REQUIRE("" == p.getName());

    p = Point(-123, 234, "test");
    REQUIRE(-123 == Approx(p.getX()));
    REQUIRE(234 == Approx(p.getY()));
    REQUIRE("test" == p.getName());
}

TEST_CASE("Point: Construction 2") {
    Point p(10, 12);

    Point p2 = p;
    REQUIRE(p.getX() == Approx(p2.getX()));
    REQUIRE(p.getY() == Approx(p2.getY()));
    REQUIRE(p.getName() == p2.getName());
}


TEST_CASE("Point: Construction 3") {
    Point p(10, 12, "Name");

    Point p2 = p;
    REQUIRE(p.getX() == Approx(p2.getX()));
    REQUIRE(p.getY() == Approx(p2.getY()));
    REQUIRE(p.getName() == p2.getName());
}

TEST_CASE("Point: Chaining") {
    Point p(10, 12, "Name");

    (((p *= 10) /= 2) += {20, 10}) -= {5, 3};

    REQUIRE(p.getX() == Approx(65));
    REQUIRE(p.getY() == Approx(67));
}

TEST_CASE("Point: Multiplication") {
    Point p(10, 12, "Name");

    Point res = p * 2;

    REQUIRE(p.getX() == Approx(10));
    REQUIRE(p.getY() == Approx(12));
}

TEST_CASE("Point: Division") {
    Point p(10, 12, "Name");

    Point res = p / 2;

    REQUIRE(res.getX() == Approx(5));
    REQUIRE(res.getY() == Approx(6));
    REQUIRE_THROWS_AS(p / 0, std::invalid_argument);
    REQUIRE_THROWS_AS(p /= 0, std::invalid_argument);
}

TEST_CASE("Point: Addition") {
    Point p(10, 12, "Name");
    Point p2(20, 30, "Name2");

    Point res = p + p2;

    REQUIRE(res.getX() == Approx(30));
    REQUIRE(res.getY() == Approx(42));
}

TEST_CASE("Point: Subtraction") {
    Point p(10, 12, "Name");
    Point p2(20, 30, "Name2");

    Point res = p - p2;

    REQUIRE(res.getX() == Approx(-10));
    REQUIRE(res.getY() == Approx(-18));
}

TEST_CASE("Point: Chainable") {
    Point p(10, 12, "Name");

    p *= 2;
    p /= 2;
    p += {20, 10};
    p -= {5, 3};

    REQUIRE(p.getX() == Approx(25));
    REQUIRE(p.getY() == Approx(19));
}

TEST_CASE("Point: Setters") {
    Point p(10, 12, "Name");

    p.setX(20);
    p.setY(30);

    REQUIRE(p.getX() == Approx(20));
    REQUIRE(p.getY() == Approx(30));
}