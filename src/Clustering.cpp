//
// Created by beatw on 7/14/2021.
//

#include "Clustering.h"

#include "TClosestPair.h"

#include <vector>

#include <random>
#include <numeric>
#include <algorithm>
#include <optional>
#include <iostream>

namespace Clustering {

    /**
     * Initialized cluster for KMeans algorithm
     *
     * Randomly selects one of the points to be the center of each cluster
     *
     * TODO: Implement pseudo code of
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> initKMeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed) {
        std::vector<Cluster> clusters;

        std::mt19937 random_engine(seed);
        std::uniform_int_distribution<int> distribution(0, pts.size() - 1);

        for (size_t i = 0; i < nbOfClusters; i++) {
            clusters.emplace_back(pts[distribution(random_engine)]);
        }

        return clusters;
    }

    /**
     * Return the closest cluster for a given point.
     *
     * TODO: Use std::min_element to implement this function.
     *
     * @param clusters
     * @param p
     * @return
     */
    Cluster &closestCluster(std::vector<Cluster> &clusters, const Point &p) {
        return *(std::min_element(clusters.begin(), clusters.end(), [&p](const Cluster &a, const Cluster &b) {
            return a.distance(p) < b.distance(p);
        }));
    }


    /**
     * Perform the KMeans clustering algorithm.
     *
     *
     * @param pts
     * @param nbOfClusters
     * @param seed
     * @return
     */
    std::vector<Cluster> kmeans(const std::vector<Point> &pts, size_t nbOfClusters, int seed) {
        std::vector<Cluster> clusters = Clustering::initKMeans(pts, nbOfClusters, seed);

        //Assign points to clusters until the clusters are stable (do not move)
        bool stable = false;
        while (!stable) {
            stable = true;
            //Assign points to clusters
            for (auto &point: pts) {
                Cluster &cluster = Clustering::closestCluster(clusters, point);
                cluster.addPoint(point);
            }
            for (auto &cluster: clusters) {
                if (cluster.recomputeCenter()) {
                    stable = false;
                }
            }
            if (!stable) {
                for (auto &cluster: clusters) {
                    cluster.clear();
                }
            }
        }

        return clusters;
    }

    /**
     * Converts all points into clusters
     * @param pts
     * @return
     */
    std::vector<Cluster> initHierchical(const std::vector<Point> &pts) {

        std::vector<Cluster> clusters;

        for (auto &point: pts) {
            clusters.emplace_back(Cluster(point));
        }

        return clusters;
    }

    /**
     * Performs a hierarchical clustering with nbOfCluster number of clusters.
     *
     * TODO: Use TClosestPair to find the closest clusters to merge
     * @param pts
     * @param nbOfClusters
     * @return
     */
    std::vector<Cluster> hierarchical(const std::vector<Point> &pts, size_t nbOfClusters) {
        std::vector<Cluster> clusters = initHierchical(pts);

        ClosestPair<Cluster> closestPair;

        //merge two the closest clusters until there are only nbOfClusters left

        while (clusters.size() > nbOfClusters) {
            auto closest = closestPair.closestPair(clusters);
            clusters.emplace_back(*closest.first, *closest.second);
            Cluster first = *closest.first, second = *closest.second;
            clusters.erase(std::find(clusters.begin(), clusters.end(), first));
            clusters.erase(std::find(clusters.begin(), clusters.end(), second));

        }
        return clusters;
    }
}