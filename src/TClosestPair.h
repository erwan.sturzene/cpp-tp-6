// Copyright 2022 Haute école d'ingénierie et d'architecture de Fribourg
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/****************************************************************************
 * @file ClosestPair.h
 * @author Erwan Sturzenegger <erwan.sturzene@edu.hefr.ch>
 *
 * @brief Declaration of the ClosestPair class.
 *
 * @date 05.04.22
* @version 0.1.0
 ***************************************************************************/
#ifndef CPPALGO_CLOSESTPAIR_H
#define CPPALGO_CLOSESTPAIR_H

#include <vector>
#include <iostream>
#include <set>
#include <utility>

#include "Point.h"

template <class T = Point>
class ClosestPair {
public:
    /**
     * @brief Construct a new Closest Pair object
     *
     * @param Ts
     */
    ClosestPair(){
        const auto comp = [](const T *a, const T *b) {
            if (a->getY() == b->getY()) {
                return a->getX() < b->getX();
            }
            return a->getY() < b->getY();
        };

        candidates = std::set<T *, std::function<bool(const T *, const T *)>>(comp);
    }
    /**
     * @brief Method to find the closest pair of Ts in a set of Ts
     *
     * @param Ts The set of Ts
     */
    std::pair<T *, T *> closestPair(std::vector<T> &searchPoints){
        //Init
        points.clear();
        for(auto &p : searchPoints){
            points.push_back(&p);
        }
        candidates.clear(); //Clear the set
        leftMostCandidate = 0; //Set the left most candidate to 0

        //Sort points by x-coordinate
        std::sort(points.begin(), points.end(), [](const Point *a, const Point *b) {
            return a->getX() > b->getX();
        });

        //Init solution
        solution = std::make_pair(points[0], points[1]);
        smallestDistance = solution.first->distance(*solution.second);

        //Search for the closest pair
        while (!points.empty()) {
            handleEvent(points.back());
            points.pop_back();
        }

        //Return the closest pair
        return solution;
    }
private:

    void handleEvent(T *p){
        shrinkCandidates(p);
        //Initiate the upper and lower bounds
        lowerSearch = {p->getX(), p->getY() - smallestDistance};
        upperSearch = {p->getX(), p->getY() + smallestDistance};

        //Get the upper and lower iterator
        auto upper_bound = candidates.upper_bound(&upperSearch);
        auto lower_bound = candidates.lower_bound(&lowerSearch);

        //Iterate over the candidates in the range
        for (auto it = lower_bound; it != upper_bound; ++it) {
            double distance = p->distance(*(*it));
            if (distance < smallestDistance) {
                //Update the solution
                solution = std::make_pair(p, *it);
                smallestDistance = distance;
            }
        }
        //Add the point to the set
        candidates.insert(p);
    }

    void shrinkCandidates(const T *p){
        while (p->getX() - points[leftMostCandidate]->getX() > smallestDistance) {
            candidates.erase(points[leftMostCandidate]);
            leftMostCandidate++;
        }
    }

    //Currently smallest distance
    double smallestDistance = std::numeric_limits<double>::infinity();
    //Current solution associated with that smallestDistance
    std::pair<T *, T *> solution;

    int leftMostCandidate = 0;

    //Temporary variables used for lower/upper bound search
    T lowerSearch = {0, 0};
    T upperSearch = {0, 0};

    std::vector<T *> points; //All Ts
    std::set<T *, std::function<bool(const T *, const T *)>> candidates; //Tree of current candidates
};

#endif //CPPALGO_CLOSESTPAIR_H
